# package-agnostic function library.  The "dtf_" prefix stands for
# "destructive test framework".

dtf_die() { echo >&2 "$@";  exit 1; }

dtf_generate_results_tarball()
{
    local dir="$(dirname "$1")"
    local name="$(basename "$1")"

    local tarball=$dir/$name.tar.gz

    pushd "$dir" >/dev/null

    local oumask="$(umask)"
    umask 0077
    tar -czf "$tarball" "$name"
    umask "$oumask"

    export DTF_RESULT_TARBALL=$tarball
}

dtf_resultxml_cache()
{
    ___dtf_resultxml_cache+="$@
"
}

dtf_resultxml_init()
{
    ___dtf_resultxml_cache=""
    ___dtf_resultxml_file="$1"
    dtf_resultxml_cache "<run><date>$(date --rfc-3339=seconds)</date>"

    if test ! -f "$___dtf_resultxml_file"; then
        echo '<?xml version="1.0" encoding="utf-8"?>' > "$___dtf_resultxml_file" \
            || dtf_die "can not create '$___dtf_resultxml_file'"
    fi
}

dtf_resultxml_finish()
{
    dtf_resultxml_cache "</run>"

    echo "$___dtf_resultxml_cache" >> "$___dtf_resultxml_file" \
        || dtf_die "can not write to $___dtf_resultxml_file"
}

create_pagila()
(
    testit()
    {
        debug "testing database"

        local cmd="psql -tA -d pagila -c \"select city from city where city = 'Banjul';\""

        out="$(admin_cmd "$cmd")"
        test "$out" = Banjul || return 1
        test "$(wc -l < pagila_init.log)" -gt 335 || return 1

        test "$({ grep ERROR | wc -l ; } < pagila_init.log)" -lt 2
    }

    debug "creating DB pagilla"
    INDENT="$INDENT  "

    pagila="pagila-0.10.1"
    pagila_tarball="$pagila.zip"
    pagila_link="http://pgfoundry.org/frs/download.php/1719/$pagila_tarball"

    cached_download $pagila_link

    debug "unzipping tarball"
    unzip $pagila_tarball &>/dev/null || die "can not unzip pagila"

    pushd $pagila >/dev/null || die "can not switch directory"
    admin_cmd "createdb pagila --owner postgres" || die "can't create db"
    { su - postgres -c 'psql -d pagila' < pagila-schema.sql \
      && su - postgres -c 'psql -d pagila' < pagila-data.sql
    } &>pagila_init.log || die "can not initialize pagila"

    cp pagila_init.log /tmp

    testit || die "can not test"

    popd >/dev/null || die "can't go back"
)

export DTF_TEST_ID="initdb-old-syntax"
export DTF_TEST_DESCRIPTION="\
Check that the old syntax 'postgresql-setup initdb' works together with
following 'service start postgresql'."

run()
{
    dtf_postgresql_test_init
    rlRun "postgresql-setup initdb"
    rlServiceStart postgresql
    dtf_postgresql_check_started
    dtf_postgresql_test_finish
}

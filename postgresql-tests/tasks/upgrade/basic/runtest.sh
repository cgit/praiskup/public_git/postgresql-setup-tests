export DTF_TEST_ID="upgrade-basic"
export DTF_TEST_DESCRIPTION="Check postgresql-setup [--]upgrade"

run()
{
    dtf_postgresql_test_init

    dtf_postgresql_upgrade_tour "$(dtf_postgresql_data_mirror)" basic.tar.gz
    rlAssert0 "test wrapper should finish successfully" $?

    dtf_postgresql_test_finish
}

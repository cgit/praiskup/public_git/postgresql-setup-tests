export dtf_dataurls dtf_libfiles

# this is needed for some tests
dtf_dataurls[0]=http://10.3.8.255/root-tests/data/postgresql/latest/
dtf_dataurls[1]=http://pensioner.lab.eng.brq.redhat.com/root-tests/data/postgresql/latest/

dtf_cb_dist_tasks()
{
    tar -ch "$dir" --exclude gen-data
}

# include PostgreSQL specific test-library
dtf_libfiles[0]="$srcdir/lib_pgsql.sh"

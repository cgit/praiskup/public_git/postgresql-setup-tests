export DTF_TEST_ID="upstream-testsuite"
export DTF_TEST_DESCRIPTION="Run the upstream testsuite available with
distribution tarball."

check_libdir()
{
    test -d "$1" || return 2
    cd "$1" || return 1
    su postgres -c "make installcheck"
}

run()
{
    local libsubdir="postgresql-odbc/test/"
    local rc=$DTF_RESULT_SUCCESS
    local resdir=

    for i in /usr/lib64 /usr/lib; do
        local workdir="$i/$libsubdir"

        check_libdir "$workdir"
        case "$?" in
            1)
                rc="$DTF_RESULT_FAILURE"
                cat "$i/regression.diffs"
                # fall through
                ;&
            0)
                resdir="$workdir"
                break
                ;;
            2)
                # skip
                ;;
        esac
            break
    done

    dtf_generate_results_tarball "$resdir" || return 1

    return "$rc"
}

#!/bin/bash

. "@sysconfdir@/dtf.sh" || exit 1

run_playbook=${run_playbook-@ansibleplaybooksdir@/default.yml}

opt_workdir=
opt_distro=fedora
opt_openstack_instance="$DTF_OPENSTACK_DEFAULT_ID"
opt_distro_ver=20
opt_extra_rpms=
opt_taskdir=
opt_setup_playbook=
opt_help=false

die() { echo >&2 "$*" ; exit 1 ; }
error() { echo >&2 "$*" ; }

longopts="help"
longopts+=",distro:,distro-version:,workdir:,openstack-instance:,extra-rpms-file:"
longopts+=",taskdir:,setup-playbook:"
ARGS=$(getopt -o "v" -l "$longopts" -n "getopt" -- "$@") \
    || exit 1
eval set -- "$ARGS"

while true; do
    case "$1" in
    --taskdir|--setup-playbook|--distro|--openstack-instance|--workdir)
        opt=$(sed -e 's/^--//' -e 's/[^[a-zA-Z0-9]/_/g'<<<"$1")
        case "$1" in
        --distro)
            # normalize
            eval "opt_$opt=\"${2,,}\""
            ;;
        *)
            eval "opt_$opt=\"${2}\""
            ;;
        esac
        shift 2
        ;;

    --help)
        opt=$(sed -e 's/^--//' -e 's/[^[a-zA-Z0-9]/_/g'<<<"$1")
        eval "opt_$opt=true"
        shift
        ;;

    --distro-version)
        opt_distro_ver="$2"
        shift 2
        ;;

    --extra-rpms-file)
        opt_extra_rpms="$(readlink -f "$2")"
        shift 2
        ;;

    --)
        shift
        break
        ;;

    *)
        echo >&2 "programmer mistake"
        exit 1
        ;;

    esac
done

$opt_help && {
cat <<EOF
Usage: $0 OPTIONS

Script which runs the dtf testsuite remotely.  For that purpose, it starts fresh
VM (currently in OpenStack), runs the testsuite and collects results.

Options:
  --taskdir=DIR              The root-directory of dtf testsuite
  --distro=DISTRO            Linux distribution name (e.g. fedora)
  --distro-version=VER       Version of DISTRO, e.g. 20
  --workdir=DIR              Do not create temporary directory for results and
                             rather use pre-created DIR

Global options:
  --openstack-instance=OID   The OS identifier matching with configuration
  --help                     Shows this help
EOF
exit 0
}

optparse_error=false
test -z "$opt_openstack_instance" \
    && error "option --openstack-instance=ID not" \
    && optparse_error=true

test -z "$opt_taskdir" \
    && error "you must specify '--taskdir DIR'" \
    && optparse_error=true

test ! -d "$opt_taskdir" \
    && error "\$opt_taskdir is not a directory '$opt_taskdir'" \
    && optparse_error=true

test -z "$opt_setup_playbook" \
    && error "you must specify '--setup-playbook PLAYBOOK'" \
    && optparse_error=true

test ! -f "$opt_setup_playbook" \
    && error "bad setup playbook '$opt_setup_playbook'" \
    && optparse_error=true

$optparse_error && exit 1

credsfile="$HOME/.dtf/private/os/$opt_openstack_instance.yml"
test ! -f "$credsfile" && die "creds file '$credsfile' not found"

config_os_file="$HOME/.dtf/os/$opt_openstack_instance.sh"
test ! -r "$config_os_file" && die "file '$config_os_file' not found"
. "$config_os_file"

config_os_id="$opt_distro$opt_distro_ver"

workdir_prereq()
{
    if test -z "$opt_workdir"; then
        opt_workdir="$(mktemp -d "/tmp/dtf-remoterun-workdir-XXXXXX")" \
            || die "can't create workdir '$opt_workdir'"
        echo "temporary workdir created: $opt_workdir"
    elif test ! -d "$opt_workdir"; then
        mkdir -p "$opt_workdir" || die "can't create workdir '$opt_workdir'"
    fi
}

tarball()
(
    testsuite_name="$(basename "$(readlink -f "$opt_taskdir")")"
    echo "$testsuite_name"
    "$opt_taskdir/run" --dist | gzip > "$opt_workdir/$testsuite_name.tar.gz"
)

workdir_prereq

testsuite_name="$(tarball)" || die "can not create dist tarball"

export ANSIBLE_HOST_KEY_CHECKING=False
ansible-playbook -v "$run_playbook" \
    --extra-vars "opt_generated_vars=@ansiblevarsdir@/generated-vars.yml" \
    --extra-vars "opt_setup_playbook='$opt_setup_playbook'" \
    --extra-vars "opt_distro=$opt_distro" \
    --extra-vars "opt_distro_ver=$opt_distro_ver" \
    --extra-vars "opt_workdir=$opt_workdir" \
    --extra-vars "opt_credsfile=$credsfile" \
    --extra-vars "os_flavor_id=${os_flavor_ids[$config_os_id]}" \
    --extra-vars "os_image_id=${os_image_ids[$config_os_id]}" \
    --extra-vars "os_keypair=${os_keypair}" \
    --extra-vars "os_security_group=${os_security_group}" \
    --extra-vars "os_network_dev=${os_network_dev}" \
    --extra-vars "opt_testsuite_name=${testsuite_name}" \
    --extra-vars "${opt_extra_rpms:+dtf_rpm_files_list=$opt_extra_rpms}"

## THIS IS EXAMPLE OPENSTACK DTF CONFIGURATION ##

# variable declarations, should be C&Ped
declare -A os_flavor_ids os_image_ids
export os_flavor_ids=[]
export os_image_ids=[]

# connection info about particular OpenStack instance

# which ssh keys should be uploaded to created VMs
export os_keypair="keypair-id"
# group of firewall rules to be set on VMs
export os_security_group=test_group
# the name of network connection as is presented by ansible
export os_network_dev=network_name

# per-distribution dictionary with images/flavors
os_flavor_ids["fedora20"]="2"
os_image_ids["fedora20"]="0461eef2-5921-4c01-95a6-6edf96d41b89"

